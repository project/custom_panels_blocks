INTRODUCTION
-------------

Custom panels blocks is a Drupal 8 module which provide the management to
add differents blocks, fields or any kind of plugins availables for the
module contrib Panels by the different roles of your platform.

It allows the possibility to add different blocks on a Panel page depends
the user role and their previous configurated permissions, therefore
which that module you can create different custom blocks selectors for
user roles.

REQUIREMENTS
-------------

 * panels module: https://www.drupal.org/project/panels

INSTALLATION
-------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
--------------

 * Configure permissions in Administration » Configuration » Custom panels
 blocks:

      Configure your blocks permissions to show or hide on Panels selector
      blocks.

 * Visit /admin/config/user-interface/custom_panels_blocks to configure
 your permissions.

MAINTAINERS
------------

Current maintainer:
 * Alberto Pérez (aperedos) - https://www.drupal.org/user/3377756
